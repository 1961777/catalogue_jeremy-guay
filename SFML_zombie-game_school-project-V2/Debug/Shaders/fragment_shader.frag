
in vec4 vert_pos;
vec2 light;
uniform sampler2D texture;
uniform bool hasTexture;
uniform vec2 lightPos;
uniform float flash;

void main()
{
	//Ambient light
	vec4 ambient = vec4(0.05, 0.05, 0.05, 0.05);
	
	//Convert light to view coords
	light = (gl_ModelViewProjectionMatrix * vec4(lightPos, 0, 1)).xy;
	
	//Calculate the vector from light to pixel (Make circular)
	vec2 lightToFrag = light - vert_pos.xy; 
	lightToFrag.y = lightToFrag.y / 1.7;

	//Length of the vector (distance)
	float vecLength = clamp(length(lightToFrag) * flash, 0, 1);

    // lookup the pixel in the texture
    vec4 pixel = texture2D(texture, gl_TexCoord[0].xy);

    // multiply it by the color and lighting
	if(hasTexture == true)
	{
		gl_FragColor = gl_Color * pixel * (clamp(ambient + vec4(1-vecLength, 1-vecLength, 1-vecLength, 1), 0, 1));
	}
	else
	{
		gl_FragColor = gl_Color;
	}
} 
/*
vec4 vert_pos;
uniform sampler2D texture;
uniform float u_time;
uniform vec2 u_resolution;
uniform vec2 lightPos;

void main(){
	vec2 coord = (gl_FragCoord.xy * 2.0 - u_resolution) / min(u_resolution.x, u_resolution.y);
	coord.y = lightPos.y;
	coord.x =  lightPos.x;

	float color = 0.0;

	color += 5 * (abs(sin(u_time)) + 5) / length(coord);

	gl_FragColor = vec4(vec3(color),5.0);
} */